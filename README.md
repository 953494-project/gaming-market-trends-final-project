# Gaming Market Trends (953494 Selected Topics in SE Project 2/2019)

## Abstract
The gaming industry has evolved and gone through spectacular changes over the last decade. Research has shown that the trends have shifted, influencing companies to focus more resources to follow along with these trends rather than ignoring it. This project aims to analyze which type of genre could be the main genres to focus on in order to match the trend of the market. Based on dozens of Kaggle solutions available online alongside with the most recent dataset possible. Dataset was picked and analyzed to identify the pattern. Analysis of the datasets demonstrated that in the most recent years, the genres as follows will most likely be the dominant genres: Action, Sports, Shooter, and Role Playing. The results indicate that there is a trend in the gaming markets towards a specific genre. In regard to this, it is recommended that gaming companies use these genres as a key factor in targeting and focusing on the design of their products. A newer dataset could prove to provide more relevant genre trends and strengthen the accuracy of the predictions.

## Members
602115001 Krittawit Khamjaroen

602115004 Jiratchaya Saopangkam

602115023 Somruk Laothamjinda

## Usage
1.) To access the code, use JupyterLab or Jupyter Notebook to access the Python code

2.) To access the website, please do the following down below (it takes a while to install all the dependencies)
```git
cd web
npm install
ng s -o
```

## Dataset used
Video Games Sales by Halim Noor

https://www.kaggle.com/mohalim/video-games-sales