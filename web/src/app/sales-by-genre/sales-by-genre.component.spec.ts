import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesByGenreComponent } from './sales-by-genre.component';

describe('SalesByGenreComponent', () => {
  let component: SalesByGenreComponent;
  let fixture: ComponentFixture<SalesByGenreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesByGenreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesByGenreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
