import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SalesByGenreComponent } from './sales-by-genre/sales-by-genre.component';
import { AlltimeSalesComponent } from './alltime-sales/alltime-sales.component';
import { IntroductionPageComponent } from './intro-page/introduction-page.component';
import { SummaryPageComponent } from './summary-page/summary-page.component';



const routes: Routes = [
  {
    path: '',
    redirectTo: '/intro',
    pathMatch: 'full'
  },
  { path: 'salesByGenre', component: SalesByGenreComponent },
  { path: 'sales', component: AlltimeSalesComponent },
  { path: 'intro',component: IntroductionPageComponent},
  { path: 'summary', component: SummaryPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
