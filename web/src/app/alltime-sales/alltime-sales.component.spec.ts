import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlltimeSalesComponent } from './alltime-sales.component';

describe('AlltimeSalesComponent', () => {
  let component: AlltimeSalesComponent;
  let fixture: ComponentFixture<AlltimeSalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlltimeSalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlltimeSalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
